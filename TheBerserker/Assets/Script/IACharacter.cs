﻿using UnityEngine;

abstract public class IACharacter : Character
    //PARENT OF SWORDS MAN AND ENEMY
{
    protected float distanceMargin;
    protected bool following;
    protected bool checkForward = true;

    abstract protected float GetTargetXPos();
    abstract protected float GetMoveSpeed();
    abstract protected Rigidbody2D GetRigidbody();
    abstract protected Animator GetAnimator();
    abstract protected bool IsAnAllyForward();
    abstract protected bool IsAnAllyBackward();

    protected bool IsFarFromTarget() { return Mathf.Abs(transform.position.x - GetTargetXPos()) > distanceMargin; }

    protected void FollowTarget()
    {
        if (transform.position.x > GetTargetXPos())
        {
            if (FacingRight)
            {
                Flip();
                FacingRight = false;
            }
            GetRigidbody().velocity = new Vector2(-GetMoveSpeed(), GetRigidbody().velocity.y);
        }
        else
        {
            if (!FacingRight)
            {
                Flip();
                FacingRight = true;
            }
            GetRigidbody().velocity = new Vector2(GetMoveSpeed(), GetRigidbody().velocity.y);
        }
    }

    protected void JustFollow() //ONLY USED IN SWORDSMAN. IT FOLLOW TARGET BUT DONT ATTACK
    { 
        if (IsFarFromTarget())
        {
            if (checkForward && !IsAnAllyForward() || !checkForward && !IsAnAllyBackward())
            {
                if (!following)
                {
                    GetAnimator().SetBool("Run", true);
                    GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
                    following = true;
                }
                FollowTarget();
            }
            else
            {
                GetAnimator().SetBool("Run", false);
                following = false;
            }
        }
        else if (following)
            {
                GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation | RigidbodyConstraints2D.FreezePositionX;
                GetAnimator().SetBool("Run", false);
                following = false;
            }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
    //COIN THAT SPAWNS FROM ENEMIES, AND CAN BE TAKEN BY PLAYER
{
    [SerializeField] private AudioClip coinSound;

    private bool invulnerable = true;
    private SpawningManager spawningManager;

    private void Start()
    {
        spawningManager = (FindObjectOfType(typeof(SpawningManager)) as SpawningManager);
        spawningManager.AddCoinToDestroyList(gameObject); //The DestroyList is a list with all objects that will be destroyed at the start of new wave
        (GetComponent(typeof(Animator)) as Animator).SetTrigger("StartRotate");
        StartCoroutine(WaitUntilStopFalling());
    }

    private IEnumerator WaitUntilStopFalling()
    {
        yield return new WaitForSeconds(0.1f); //wait a little bit to still be invulnerable when spawn
        invulnerable = false;
        //wait until the coin start to falling down (because of the gravity) and then it freeze the Z and Y position
        while (GetComponent<Rigidbody2D>().velocity.y >= 0) 
            yield return new WaitForEndOfFrame();
        GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezePositionX;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        PlayerCharacter player = other.gameObject.GetComponent<PlayerCharacter>();
        if (!invulnerable)
            if (player)
            {
                spawningManager.RemoveCoinFromDestroyList(gameObject);
                player.PlayerCoins += 10;
                AudioSource.PlayClipAtPoint(coinSound, Camera.main.transform.position, .5f);
                Destroy(gameObject);
            }
    }
}

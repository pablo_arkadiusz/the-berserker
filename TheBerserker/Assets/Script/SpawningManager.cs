﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawningManager : MonoBehaviour
{
    [SerializeField] private GameObject enemyPrefab;
    [SerializeField] private Transform enemyParent;
    [SerializeField] private ItemSpawner itemSpawner;
    [SerializeField] private Animator wavePanelAnimator;
    [SerializeField] InformationPanel informationPanel;

    private Transform[] spawningZone;
    private const float spawningTime = 0.75f;
    private uint enemiesAlive;
    private bool allWaveSpawned;
    public uint Wave { private set; get; }

    private List<GameObject> enemiesToDestroy = new List<GameObject>();
    private List<GameObject> coinsToDestroy = new List<GameObject>();
    void Start ()
    {
        spawningZone = new Transform[transform.childCount];
        for(byte i=0;i< transform.childCount;++i)
            spawningZone[i] = transform.GetChild(i);

        StartSpawningWave();
    }

    private IEnumerator SpawnEnemies(uint num)
    {
        sbyte direction = 1;
        float force;
        float enemyLife = 100 + (Wave * 50);
        float enemyDamage = 20;

        for (byte i = 0; i < num; ++i)
        {
            for (uint j = 0; j < spawningZone.Length; ++j)
            {
                Vector3 posToIntantiate = new Vector3(spawningZone[j].position.x, spawningZone[j].position.y, enemyParent.position.z);
                force = Random.Range(-350, 350);
                Rigidbody2D enemyRB = Instantiate(enemyPrefab, posToIntantiate, Quaternion.identity) /*----------*/ .GetComponent(typeof(Rigidbody2D)) as Rigidbody2D;
                enemyRB.transform.parent = enemyParent;
                enemyRB.AddForce(new Vector2(force * direction, force));
                Enemy enemy = enemyRB.GetComponent<Enemy>();
                enemy.SpawningManager = this;
                enemy.Life = enemyLife;
                enemy.Damage = enemyDamage;
                direction *= -1;
                ++enemiesAlive;
                UpdateText(enemiesAlive);
            }
            yield return new WaitForSeconds(spawningTime);
        }
        allWaveSpawned = true;
        informationPanel.Entry();
        CheckForNextWave();
    }

    private void CheckForNextWave()
    {
        if (enemiesAlive == 0 && allWaveSpawned)
        {
            allWaveSpawned = false;
            ++Wave;
            //wavePanelAnimator.SetBool("Entry", true);
            informationPanel.Move();
            StartCoroutine(SendInformationPanelTopAndStartCounting());
        }
    }

    private IEnumerator SendInformationPanelTopAndStartCounting()
    {
        informationPanel.UpdateActuaWaveTxt(Wave.ToString());
        yield return new WaitForSeconds(3);
        informationPanel.GoTop();
        StartCoroutine(StartCountingTime(10));
    }

    private IEnumerator StartCountingTime(byte seconds)
    {
        for(; seconds!=0;--seconds)
        {
            informationPanel.UpdateSecsLeftText(seconds);
            yield return new WaitForSeconds(1);
        }
        informationPanel.Exit();
        StartSpawningWave();
    }

    private void StartSpawningWave()
    {
        DestroyDeadEnemies();
        DestroyCoins();
        itemSpawner.DeleteLastSpawnedItems();
        itemSpawner.SpawnLifeAndManaBonus();
        uint enemiesToSpawnFoEachSpawnPoint = 5 + Wave * 4;
        StartCoroutine(SpawnEnemies(enemiesToSpawnFoEachSpawnPoint));
    }

    public void RegisterEnemyDeath()
    {
        --enemiesAlive;
        UpdateText(enemiesAlive);
        CheckForNextWave();
    }

    private void DestroyDeadEnemies()
    {
        foreach (GameObject enemy in enemiesToDestroy)
            Destroy(enemy);
    }
    private void DestroyCoins()
    {
        foreach (GameObject coin in coinsToDestroy)
            Destroy(coin);
    }

    private void UpdateText(uint num) { informationPanel.UpdateEnemiesLeft(num); }
    public void AddEnemyToDestroyList(GameObject enemyToDestroy) { enemiesToDestroy.Add(enemyToDestroy); }
    public void AddCoinToDestroyList(GameObject coinToDestroy) { coinsToDestroy.Add(coinToDestroy); }
    public void RemoveCoinFromDestroyList(GameObject coin) { coinsToDestroy.Remove(coin); }
}

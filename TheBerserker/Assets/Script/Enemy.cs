﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : IACharacter
    /*ENEMY FOCUS/TARGET THE NEAREST CHARACTER(PLAYER OR SWORDSMAN) AND IF IT IS FAR, IT GET CLOSER. IF ITS CLOSE IT CHARGE THE COOLDOWN-BAR AND SHOOT*/
{
    [Header("Enemy Stats")]
    [SerializeField] private float moveSpeed = 2f;
    [SerializeField] private float cooldownTime = 8f;
    [SerializeField] private float checkEnemyRatio;

    [Header("Components")]
    [SerializeField] private Rigidbody2D myRigidBody;
    [SerializeField] private Animator myAnimator;

    [Header("Prefabs")]
    [SerializeField] private GameObject spellPrefab;
    [SerializeField] private GameObject coinPrefab;

    [Header("Children GameObjects")]
    [SerializeField] private Transform spellPoint;
    [SerializeField] private GameObject lifeBar;
    [SerializeField] private GameObject cooldownBar;

    [Header("Layer Mask")]
    [SerializeField] private LayerMask allyLayer;
    [SerializeField] private LayerMask whatIsHisAlly;

    [Header("Chest Zone")]
    [SerializeField] private Transform chestZoneA;
    [SerializeField] private Transform chestZoneB;

    //NonSerializable Stats Variables and properties
    public float Damage { set; private get; } //Damage setted by the SpawnManager
    private float startingLife;
    override public float Life { set; get; }
    override protected float GetMoveSpeed() { return moveSpeed; }

    //target varables
    private Transform target; //target can be Player or SwordsMan. It focus the nearest
    override protected float GetTargetXPos() { return target.position.x; }
    private PlayerCharacter player;

    //NonSerializable Children GameObject Variables
    private Image lifeBarImg;
    private Image cooldownBarImg;

    //NonSerializable Component Variables
    override protected Rigidbody2D GetRigidbody() { return myRigidBody; }
    override protected Animator GetAnimator() { return myAnimator; }

    //CONT VARIABLES
    private const float ATTACK_ANIMATION_DURATION = 0.2f;
    private const byte DEATH_LAYER = 14;


    private Coroutine followingToAttack;

    public SpawningManager SpawningManager { set; private get; }

    private bool preparingToAttack;//ITS USED TO START ATTACK ONLY ONCE PER COOLDOWN

    private void Start()
    {
        cooldownBarImg = cooldownBar.transform.GetChild(1).GetComponent(typeof(Image)) as Image;
        lifeBarImg = lifeBar.transform.GetChild(1).GetComponent(typeof(Image)) as Image;
        startingLife = Life;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent(typeof(PlayerCharacter)) as PlayerCharacter;
        LookForPlayerAsTarget();
        distanceMargin = 6f;
        UpdateLifeBar();
        StartCoroutine(StartFollowInSsecs(4));
        StartCoroutine(LookForCloserEnemy());
    }

    private IEnumerator StartFollowInSsecs(float secs) //WHEN IT SPAWN WAIT SOME SECONDS BEFORE START MOVING
    {
        StartCoroutine(AttackInSite(secs));
        yield return new WaitForSeconds(secs);
        if(!Died)
            followingToAttack = StartCoroutine(FollowToAttack());
    }

    private IEnumerator AttackInSite(float secs) //WHEN ENEMY SPAWN HE CAN ATTACK (IF ITS CLOSE TO TARGET) BUT NOT MOVE
    {
        float startTime = Time.time;
        while (Time.time - startTime < secs && !Died)
        {
            if (!IsFarFromTarget())
            {
                myRigidBody.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
                if (target.position.x > transform.position.x && !FacingRight)
                {
                    FacingRight = true;
                    Flip();
                }
                else if (target.position.x < transform.position.x && FacingRight)
                {
                    FacingRight = false;
                    Flip();
                }
                if (following)
                {
                    GetAnimator().SetBool("Run", false);
                    following = false;
                }
                if (!preparingToAttack)
                    TryAttack();
            }
            yield return new WaitForEndOfFrame();
        }
    }

    private void NotifyEnemySpawner() { SpawningManager.RegisterEnemyDeath(); }
    private void UpdateLifeBar() { lifeBarImg.fillAmount = Life / startingLife; }
    //METHODS THAT CHECK IF AN ALLY IS BACKWARD OR FORWARD
    override protected bool IsAnAllyBackward() { return true; }
    override protected bool IsAnAllyForward() 
    {
        Collider2D[] ally = Physics2D.OverlapAreaAll(chestZoneA.position, chestZoneB.position, whatIsHisAlly);
        return ally.Length > 1;
    }




    private IEnumerator ChargeCooldownBar() //ENEMY CHARGE THE COOLDOWN ONLY IF IS CLOSE TO THE TARGET
    {
        float chargeAmount = 0.01f / cooldownTime;
        while (!IsFarFromTarget() && cooldownBarImg.fillAmount<1)
        {
            cooldownBarImg.fillAmount += chargeAmount;
            yield return new WaitForSeconds(0.01f);
        }
        if (cooldownBarImg.fillAmount >= 1 && !Died)
        {
            myAnimator.SetTrigger("Attacking");
            StartCoroutine(Shoot());
        }
        else preparingToAttack = false;
    }

    new public void TakeDamage(float dmg)
    {
        base.TakeDamage(dmg);
        UpdateLifeBar();
    }
    
    override protected void TryAttack()
    {
        preparingToAttack = true;
        StartCoroutine(ChargeCooldownBar());
    }

    private IEnumerator Shoot()
    {
        cooldownBarImg.fillAmount = 0;
        yield return new WaitForSeconds(ATTACK_ANIMATION_DURATION); //WAIT THE ANIM DURATION, TO SINCRONICE THE BULLET SPAWN AND THE ANIMATION
        preparingToAttack = false;
        
        Spell spell = Instantiate(spellPrefab, spellPoint.position, Quaternion.identity) /*-----------*/ .GetComponent(typeof(Spell)) as Spell;
        spell.SetTarget(target);
        spell.Damage = Damage;
        spell.transform.parent = transform.parent;
    }

    override protected void Die()
    {
        MakeBarsInvisible();
        SpawnCoin();
        Died = true;
        if(followingToAttack!=null) StopCoroutine(followingToAttack);
        myAnimator.SetTrigger("Die");
        gameObject.layer = DEATH_LAYER;
        NotifyEnemySpawner();

        SpawningManager.AddEnemyToDestroyList(gameObject);
        StopAllCoroutines();
    }

    private void MakeBarsInvisible() //WHEN ENEMY DIE THE HEATH AND MANA BAR DISAPPEAR
    {
        lifeBar.SetActive(false);
        cooldownBar.SetActive(false);
    }

    private void LookForPlayerAsTarget() //IF THERE ARENT ANY SWORDSMAN, ENEMY FOCUS PLAYER
    {
        if (!player.Died)
        {
            target = player.transform.Find("PlayerPivot").transform;

            if (!FacingRight && target.transform.position.x > transform.position.x || FacingRight && target.transform.position.x < transform.position.x)
            {
                FacingRight = !FacingRight;
                Flip();
            }
        }
        else Die();
    }

    protected IEnumerator FollowToAttack()
    {
        while (true)
        {
            if (target.parent.GetComponent<Character>().Died)
                LookForPlayerAsTarget();
            if (IsFarFromTarget()) //IF THE TARGET IS FAR, THE ENEMY GET CLOSER
                if (!IsAnAllyForward())
                {
                    if (!following) //IF IT JUST STARTED THE FOLLOW
                    {
                        myRigidBody.constraints = RigidbodyConstraints2D.FreezeRotation;
                        GetAnimator().SetBool("Run", true);
                        following = true;
                    }
                    FollowTarget();
                }
                else //IF THERE IS ANOTHER ENEMY IN THE CHEST ZONE. IT STOP
                {
                    myRigidBody.constraints = RigidbodyConstraints2D.FreezeRotation | RigidbodyConstraints2D.FreezePositionX;
                    GetAnimator().SetBool("Run", false);
                    following = false;
                }
            else //IF THE TARGET IS CLOSE THE ENEMY ATTACKS
            {
                GetRigidbody().constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
                if (target.position.x > transform.position.x && !FacingRight)
                {
                    FacingRight = true;
                    Flip();
                }
                else if (target.position.x < transform.position.x && FacingRight)
                {
                    FacingRight = false;
                    Flip();
                }
                if (following)
                {
                    GetAnimator().SetBool("Run", false);
                    following = false;
                }
                if (!preparingToAttack)
                    TryAttack();
            }
            yield return new WaitForEndOfFrame();
        }
    }



    private void SpawnCoin()
    {
        float randomForce = Random.Range(-500, 500);
        GameObject coin = Instantiate(coinPrefab, transform.position, Quaternion.identity);
        (coin.GetComponent(typeof(Rigidbody2D)) as Rigidbody2D).AddForce(new Vector2(randomForce,500));
        coin.transform.parent = transform.parent;
        Vector3 position = new Vector3(transform.localPosition.x, transform.localPosition.y, coin.transform.position.z);
        coin.transform.localPosition = position;
    }

    new protected void Flip()
    {
        base.Flip();

        Vector3 theScale = cooldownBarImg.transform.localScale;
        theScale.x *= -1;
        lifeBarImg.transform.localScale = cooldownBarImg.transform.localScale = theScale;
    }

    private IEnumerator LookForCloserEnemy()
    {
        while (true)
        {
            Collider2D[] allyCollider = Physics2D.OverlapCircleAll(transform.position, checkEnemyRatio, allyLayer);
            Transform newTarget = null;
            float closerDistance = float.MaxValue;
            if (allyCollider.Length>0)
            {
                foreach(Collider2D collider in allyCollider)
                {
                    if (Mathf.Abs(transform.position.x - collider.transform.position.x) < closerDistance)
                    {
                        closerDistance = Mathf.Abs(transform.position.x - collider.transform.position.x);
                        newTarget = collider.transform;
                    }
                }
                float distToPlayer = Mathf.Abs(transform.position.x - player.transform.position.x);
                if (closerDistance < distToPlayer)
                {
                    target = newTarget.Find("PivotPoint");
                    if (!FacingRight && target.transform.position.x > transform.position.x || FacingRight && target.transform.position.x < transform.position.x)
                    {
                        FacingRight = !FacingRight;
                        Flip();
                    }
                }
            }
            yield return new WaitForSeconds(1);
        }
    }


    private void OnDrawGizmosSelected() //SHOW THE CHEST ZONE AND THE ENEMY RATIO IN THE GIZMOS WHEN SELECTED
    {
        Gizmos.color = Color.blue;
        Vector2 pointA = chestZoneA.transform.position;
        Vector2 pointB = chestZoneB.transform.position;
        Vector2 pointC = new Vector3(chestZoneA.transform.position.x, chestZoneB.transform.position.y);
        Vector2 pointD = new Vector3(chestZoneB.transform.position.x, chestZoneA.transform.position.y);
        Gizmos.DrawLine(pointA, pointC);
        Gizmos.DrawLine(pointC, pointB);
        Gizmos.DrawLine(pointB, pointD);
        Gizmos.DrawLine(pointD, pointA);

        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, checkEnemyRatio);
    }
}
﻿using UnityEngine;

abstract public class Character : MonoBehaviour
    //THE PARENT OF IACHARACTER(SWORDSMAN AND ENEMY) AND PLAYER.
{
    //Properties
    protected bool FacingRight { set; get; }
    protected bool AttackCooldown { set; get; }
    public bool Died { protected set; get; }

    //abstract members
    abstract protected void TryAttack();
    abstract public float Life { set; get; }
    abstract protected void Die();

    protected void Flip()
    {
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    public void TakeDamage(float damage)
    {
        Life -= damage;
        if (Life <= 0)
            Die();
    }
}

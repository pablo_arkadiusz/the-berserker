﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    enum Scene : byte { MENU, GAME }

    private Image fadeImg;

    private void Awake() {fadeImg = GetComponentInChildren(typeof(Image)) as Image;}

    private void Update()
    {
        transform.position = Camera.main.transform.position; //THE FADE SPRITE HAVE TO BE ALWAYS ON THE CAMERA
    }

    public void LoadGame()
    {
        StartCoroutine(FadeAndLoad((byte)Scene.GAME));
    }
    public void LoadMenu()
    {
        StartCoroutine(FadeAndLoad((byte)Scene.MENU));
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    void OnLevelWasLoaded(int level)
    {
        StartCoroutine(FadeOut());
    }

    private IEnumerator FadeAndLoad(byte sceneID)
    {
        Color newColor = new Color(0, 0, 0, 0.05f);
        while (fadeImg.color.a < 1)
        {
            fadeImg.color += newColor;
            yield return new WaitForEndOfFrame();
        }
        SceneManager.LoadScene(sceneID);
    }

    private IEnumerator FadeOut()
    {
        fadeImg.color = Color.black;
        Color newColor = new Color(0, 0, 0, 0.03f);
        fadeImg = GetComponentInChildren(typeof(Image)) as Image;
        while (fadeImg.color.a >= 0)
        {
            fadeImg.color -= newColor;
            yield return new WaitForEndOfFrame();
        }
    }
}

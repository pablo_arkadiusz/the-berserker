﻿using UnityEngine;
using UnityEngine.UI;

public class LosePanel : MonoBehaviour
    //THE PANEL THAT APPEARS WHEN PLAYER DIE
{
    [SerializeField] private SpawningManager spawningManager;
    [SerializeField] private Text loseText;

    public void Entry(){(GetComponent(typeof(Animator)) as Animator).SetTrigger("Entry");}
    public void UpdateText(){loseText.text = "You have survived " + spawningManager.Wave + " waves"; }


}

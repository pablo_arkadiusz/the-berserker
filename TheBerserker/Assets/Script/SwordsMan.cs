﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordsMan : IACharacter
{
   
    [SerializeField] private float life = 100;
    [SerializeField] private float damage;
    [SerializeField] private Rigidbody2D myRigidBody;
    [SerializeField] private Animator myAnimator;
    [SerializeField] private float moveSpeed = 2f;
    private Character target;
    [SerializeField] private float checkEnemyRatio = 5f;
    [SerializeField] private LayerMask whatIsEnemy;
    [SerializeField] private LayerMask whatIsAlliance;
    [SerializeField] private float attackRange;
    [SerializeField] private Transform damageZoneA;
    [SerializeField] private Transform damageZoneB;
    [SerializeField] private Transform chestZoneA;
    [SerializeField] private Transform chestZoneB;
    [SerializeField] private Transform backZoneA;
    [SerializeField] private Transform backZoneB;
    private SpawningManager spawnerManager;
    private Character player;
    //----------------------------------//
    public enum State { Normal, Attack };

    //----------------------------------//
    private const float ATTACK_ANIMATION_DURATION = 0.2f;
    private const float DISTANCE_TO_PLAYER = 5f;
    private const float DISTANCE_TO_ENEMY = 1f;
    //----------------------------------//

    public State actualState;
    //private bool following;
    private bool facingRight;

    override protected float GetTargetXPos() { return target.transform.position.x; }
    override protected float GetMoveSpeed() { return moveSpeed; }
    override protected Rigidbody2D GetRigidbody() { return myRigidBody; }
    override protected Animator GetAnimator() { return myAnimator; }
    override public float Life { set { life = value; } get { return life; } }

    private Coroutine actionCoroutine;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player") /*-----------------------------*/ .transform.GetComponent(typeof(Character)) as Character;
        spawnerManager = FindObjectOfType(typeof(SpawningManager)) as SpawningManager;
    }

    private void Start()
    {
        StartCoroutine(SetConstraitsFirstTimeOnLand());
    }

    private IEnumerator NormalStateAction()
    {
        while(true)
        {
            if (!FacingRight && player.transform.position.x > transform.position.x || FacingRight && player.transform.position.x < transform.position.x)
                checkForward = false;
            else checkForward = true;
            JustFollow();
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator ActionStateAction()
    {
        while (true)
        {
            if (target && !target.Died)
                FollowToAttack();
            else
                BackToNormalState();
            yield return new WaitForEndOfFrame();
        }
    }

 


    private IEnumerator SetConstraitsFirstTimeOnLand()
    {
       
        yield return new WaitForSeconds(0.5f);
        while (myRigidBody.velocity.y < 0)
            yield return new WaitForEndOfFrame();

        
        BackToNormalState();
        if (IsFarFromTarget()) {  myRigidBody.constraints = RigidbodyConstraints2D.FreezeRotation; }
        else { myRigidBody.constraints = RigidbodyConstraints2D.FreezeRotation| RigidbodyConstraints2D.FreezePositionX; }
        
        StartCoroutine(CheckForEnemyEverySec());
        
        
        
    }


    protected void FollowToAttack()
    {  
            if (IsFarFromTarget())
            {
                if (!IsAnAllyForward())
                {
                    if (!following)
                    {
                        GetAnimator().SetBool("Run", true);
                        myRigidBody.constraints = RigidbodyConstraints2D.FreezeRotation;
                        following = true;
                        
                }
                    FollowTarget();
                }
                else GetAnimator().SetBool("Run", false);
            }
            else if (!IsFarFromTarget())
            {

                if (following)
                {
                    GetAnimator().SetBool("Run", false);
                    myRigidBody.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
                    following = false;
                }
                TryAttack();
            }
            if (!FacingRight && target.transform.position.x > transform.position.x || FacingRight && target.transform.position.x < transform.position.x)
            {
                FacingRight = !FacingRight;
                Flip();
            }
    }

    override protected void TryAttack()
    {
        if (!AttackCooldown)
        {
            AttackCooldown = true;
           
            StartCoroutine(DoDamage());
        }
    }

    private IEnumerator DoDamage()
    {
        if (target && !target.GetComponent<Character>().Died)
        {
            myAnimator.SetBool("Attacking", true);
            while (myAnimator.GetCurrentAnimatorStateInfo(0).IsTag("CanJump"))
            {
                if (!target || target.GetComponent<Character>().Died)
                {
                    myAnimator.SetBool("Attacking", false);
                    AttackCooldown = false;
                    yield break;
                }
                yield return new WaitForEndOfFrame();
            }

            yield return new WaitForSeconds(ATTACK_ANIMATION_DURATION);
            
            if (target.GetType() == typeof(PlayerCharacter))
            {
                myAnimator.SetBool("Attacking", false);
                AttackCooldown = false;
                yield break;
            }

            if (!target || target.Died) { BackToNormalState(); myAnimator.SetBool("Attacking", false); yield break; }
            Collider2D[] enemiesToDamage = Physics2D.OverlapAreaAll(damageZoneA.position, damageZoneB.position, whatIsEnemy);
            foreach (Collider2D enemyColider in enemiesToDamage)
                (enemyColider.GetComponent(typeof(Enemy)) as Enemy).TakeDamage(damage);
            //Wait for end of attack animation
            myAnimator.SetBool("Attacking", false);
            while (!myAnimator.GetCurrentAnimatorStateInfo(0).IsTag("CanJump"))
                yield return new WaitForEndOfFrame();
   
            AttackCooldown = false;
        }
    }

    private void BackToNormalState()
    {
        actualState = State.Normal;
        target = player;
        distanceMargin = DISTANCE_TO_PLAYER;
        //StartCoroutine(CheckForEnemyEverySec());

        if (IsFarFromTarget()) { following = false; }
        else { following = true; }
        if (actionCoroutine != null)
            StopCoroutine(actionCoroutine);
        actionCoroutine = StartCoroutine(NormalStateAction());
    }

    protected override void Die()
    {
        if (actionCoroutine != null)
            StopCoroutine(actionCoroutine);
        Died = true;
        gameObject.layer = 14;
        myAnimator.SetTrigger("Die");
        Vector3 pos = new Vector3(transform.localPosition.x, transform.localPosition.y, 0);
        transform.localPosition = pos;
        spawnerManager.AddEnemyToDestroyList(gameObject);
    }

    private void StartAttackState(Character newTarget)
    {
        actualState = State.Attack;
        target = newTarget;
        distanceMargin = DISTANCE_TO_ENEMY;


        if (!FacingRight && target.transform.position.x > transform.position.x || FacingRight && target.transform.position.x < transform.position.x)
        {
            FacingRight = !FacingRight;
            Flip();
        }
        
            if (IsFarFromTarget()) following = false;
            else following = true;

        if (actionCoroutine != null)
            StopCoroutine(actionCoroutine);
        actionCoroutine = StartCoroutine(ActionStateAction());

    }

    private IEnumerator CheckForEnemyEverySec()
    {
        while (!Died)
        {
            Collider2D[] enemyCollider = Physics2D.OverlapCircleAll(transform.position, checkEnemyRatio, whatIsEnemy);

            if (enemyCollider.Length>0)
            {
                float closerDistance = float.MaxValue;
                Character closerEnemy=null;
                foreach (Collider2D collider in enemyCollider)
                    if (Mathf.Abs(transform.position.x - collider.transform.position.x) < closerDistance && collider.transform.localPosition.y < -3)
                    {
                        closerDistance = Mathf.Abs(transform.position.x - collider.transform.position.x);
                        closerEnemy = collider.GetComponent(typeof(Character)) as Character;
                    } 
                
                if (closerEnemy != null )
                {
                    if(actualState == State.Normal)
                        StartAttackState(closerEnemy);
                    target = closerEnemy;
                    if (!FacingRight && target.transform.position.x > transform.position.x || FacingRight && target.transform.position.x < transform.position.x)
                    {
                        FacingRight = !FacingRight;
                        Flip();
                    }
                }
            }
            yield return new WaitForSeconds(1);
        }
    }

   override protected bool IsAnAllyForward()
    {
        Collider2D[] ally = Physics2D.OverlapAreaAll(chestZoneA.position, chestZoneB.position, whatIsAlliance);
        return ally.Length > 1;
    }

    override protected bool IsAnAllyBackward()
    {
        Collider2D[] ally = Physics2D.OverlapAreaAll(backZoneA.position, backZoneB.position, whatIsAlliance);
        return ally.Length > 1;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;

        Vector2 pointA = chestZoneA.transform.position;
        Vector2 pointB = chestZoneB.transform.position;
        Vector2 pointC = new Vector3(chestZoneA.transform.position.x, chestZoneB.transform.position.y);
        Vector2 pointD = new Vector3(chestZoneB.transform.position.x, chestZoneA.transform.position.y);
        Gizmos.DrawLine(pointA, pointC);
        Gizmos.DrawLine(pointC, pointB);
        Gizmos.DrawLine(pointB, pointD);
        Gizmos.DrawLine(pointD, pointA);

        pointA = backZoneA.transform.position;
        pointB = backZoneB.transform.position;
        pointC = new Vector3(backZoneA.transform.position.x, backZoneB.transform.position.y);
        pointD = new Vector3(backZoneB.transform.position.x, backZoneA.transform.position.y);
        Gizmos.DrawLine(pointA, pointC);
        Gizmos.DrawLine(pointC, pointB);
        Gizmos.DrawLine(pointB, pointD);
        Gizmos.DrawLine(pointD, pointA);


        Gizmos.color = Color.red;
        pointA = damageZoneA.transform.position;
        pointB = damageZoneB.transform.position;
        pointC = new Vector3(damageZoneA.transform.position.x, damageZoneB.transform.position.y);
        pointD = new Vector3(damageZoneB.transform.position.x, damageZoneA.transform.position.y);
        Gizmos.DrawLine(pointA, pointC);
        Gizmos.DrawLine(pointC, pointB);
        Gizmos.DrawLine(pointB, pointD);
        Gizmos.DrawLine(pointD, pointA);


        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, checkEnemyRatio);
    }
}

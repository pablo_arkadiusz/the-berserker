﻿using UnityEngine;

public class PauseMenu : MonoBehaviour
    //SCRIPT THAT MANAGE THE PAUSE MENU PANEL
{
    public static bool GamePaused;

    private void Awake()
    {
        GamePaused = false;
        Time.timeScale = 1;
    }
    
	public void Entry()
    {
        (GetComponent(typeof(Animator)) as Animator).SetBool("Paused", true);
        GamePaused = true;
        Time.timeScale = 0;
    }

    public void Leave()
    {
        (GetComponent(typeof(Animator)) as Animator).SetBool("Paused", false);
        GamePaused = false;
        if(!ShopDoor.shopPause)
            Time.timeScale = 1;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpell : MonoBehaviour
{
    //THE SPELL THAT PLAYER INSTANTIATE WHEN USE UPPERCUT

    [SerializeField] private AudioClip skillSound;
    [SerializeField] private float speed;
    [SerializeField] private float timeLife;

    public bool Right { set; private get; }
    private sbyte direction;
    public float Damage { set; private get; }
    
    private void Start()
    {
        AudioSource.PlayClipAtPoint(skillSound, Camera.main.transform.position, 1f);
        StartCoroutine(DestroyInSecs());
        direction = Right ? (sbyte)1 : (sbyte)-1;
        if (Right) Flip();
    }

    private void Update()
    {
        transform.Translate(new Vector2(speed * direction, 0));
    }

    protected void Flip()
    {
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        (other.GetComponent(typeof(Enemy)) as Enemy).TakeDamage(Damage);
    }

    private IEnumerator DestroyInSecs()
    {
        yield return new WaitForSeconds(timeLife);
        Destroy(gameObject);
    }
}

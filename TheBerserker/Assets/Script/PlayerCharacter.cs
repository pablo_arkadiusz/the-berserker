﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : Character
{
    [Header("Audio")]
    [SerializeField] private AudioClip swordSound;
    [SerializeField] private AudioClip skillSound;
    [SerializeField] private AudioClip hurtSound;

    [Header("Stats")]
    [SerializeField] private float life = 100f;
    [SerializeField] private float moveSpeed = 10f;
    [SerializeField] private float jumpForce;
    [SerializeField] private float damage;
    [SerializeField] private float attackRange;
    
    [Header("Components")]
    [SerializeField] private Animator myAnimator;
    [SerializeField] private Rigidbody2D myRigidBody;
    [SerializeField] private Transform damageZoneA;
    [SerializeField] private Transform damageZoneB;
    [SerializeField] private GameObject thrusterImage;
    [SerializeField] private GameObject UpperCutImage;
    [SerializeField] private GameObject spellPrefab;
    [SerializeField] private GameObject auraShield;

    [Header("Ground Info")]
    [SerializeField] private LayerMask whatIsGround;
    [SerializeField] private float GROUND_RADIUS = 0.2f;
    [SerializeField] private Transform[] groundPoint;

    [SerializeField] private LayerMask whatIsEnemy;

    private FillBarManager fillBarManager;
    private int playerCoins;
    public int PlayerCoins { set { playerCoins = value;  fillBarManager.UpdateCoinText();  } get { return playerCoins; } }
    private const byte MAX_UPGRADE = 3;
    private const float DMG_FOR_UPGRADE = 100;
    private byte upgrade=1;

    public byte Upgrade
    {
        set
        {
            if (value > MAX_UPGRADE || value != upgrade+1) throw new System.Exception("bad upgrade value: " + value);
            upgrade = value;
            GetComponent<Spine.Unity.SkeletonAnimation>().skeleton.SetSkin("lv" + upgrade);
            damage += DMG_FOR_UPGRADE;
        }
        get{return upgrade;}
    }

    //-------------------//
    private const float ATTACK_ANIMATION_DURATION = 0.2f;
    private bool sitting;
    private bool jump;
    private bool secondJump;
    private bool usingThrustSkill;
    private bool usingSpellSkill;
    private bool canShield = true;
    private float horizontalMovement;
    public const float MAX_LIFE = 100;
    public const byte MAX_MANA = 6;
    private byte actualMana;
    public byte ActualMana { private set {  actualMana = (value > MAX_MANA) ? MAX_MANA : value; } get { return actualMana; } }

    override public float Life{ set {life=value;} get { return life; } }

    public bool Shielding { get; private set; }

    private void Start()
    {
        ActualMana = MAX_MANA;
        fillBarManager = FindObjectOfType(typeof(FillBarManager)) as FillBarManager;
    }

    void Update ()
    {
        HandleClicks(); //SPECIFY WHAT EVERY CLICK DOES
    }

    private void FixedUpdate()
    {
        ResetTriggers(); //RESET THE TRIGGERS EVERY FRAME TO DONT ACUMULATE THE PREVIOUS CLICK, (FOR EXAMPLE CLICKED ATTACK DURING GUARD IS DELETED)
        
        horizontalMovement = Input.GetAxis("Horizontal");
        HandleMovement(); 
    }

    private IEnumerator CheckUntilGrounded()
    {
        while(myRigidBody.velocity.y >= -0.1f)
            yield return new WaitForEndOfFrame();
        while (!IsGrounded())
            yield return new WaitForEndOfFrame();
        
        myAnimator.SetBool("Jump", false);
        secondJump = false;
        jump = false;
    }
    
    private void HandleClicks()
    {
        if (Input.GetMouseButtonDown(0))
            TryAttack();
        if (Input.GetKeyDown(KeyCode.W))
            HandleJump();
        if (Input.GetKey(KeyCode.S))
            Sit();
        if (Input.GetKeyUp(KeyCode.S))
            DontSit();
        if (Input.GetKeyDown(KeyCode.A) && !Input.GetKey(KeyCode.D))
            myAnimator.SetBool("Run", true);
        if (Input.GetKeyDown(KeyCode.D) && !Input.GetKey(KeyCode.A))
            myAnimator.SetBool("Run", true);
        if ((Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D)) && (!Input.GetKey(KeyCode.D) && !Input.GetKey(KeyCode.A)) || Input.GetKeyUp(KeyCode.S) && (!Input.GetKey(KeyCode.D) && !Input.GetKey(KeyCode.A)))
            myAnimator.SetBool("Run", false);
        if (Input.GetMouseButtonDown(1))
            ProtectWithShield();
        if (Input.GetKeyDown(KeyCode.C))
            myAnimator.SetTrigger("Casting");
        if (Input.GetKeyDown(KeyCode.X))
            myAnimator.SetTrigger("Hit");
        if (Input.GetKeyDown(KeyCode.V))
            if (ActualMana > 0) ThrustSkill();
        if (Input.GetKeyDown(KeyCode.B) || Input.GetKeyDown(KeyCode.Space))
            if (ActualMana > 0) UpperCut();
    }

    private void Sit()
    {
        bool canSit = !sitting && !AttackCooldown && !usingSpellSkill && !usingThrustSkill && !Shielding;
        if (canSit)
        {
            sitting = true;
            myAnimator.SetBool("Sit", true);
            fillBarManager.IncreaseManaChargeSpeed();
        }
    }
    bool onlyOnce=true;
    private void DontSit()
    {
        if (sitting && onlyOnce)
        {
            onlyOnce = false;
            myAnimator.SetBool("Sit", false);
            fillBarManager.BackToNormalManaChargeSpeed();
            StartCoroutine(WaitSitEnd());
        }
    }
    private IEnumerator WaitSitEnd()
    {
        while (!myAnimator.GetCurrentAnimatorStateInfo(0).IsTag("CanJump"))
            yield return new WaitForEndOfFrame();
        yield return new WaitForSeconds(0.45f);
        onlyOnce = true;
        sitting = false;
    }
    private void ResetTriggers()
    {
        myAnimator.ResetTrigger("Casting");
        myAnimator.ResetTrigger("Hit");
        myAnimator.ResetTrigger("vSkill");
        myAnimator.ResetTrigger("UpperCut");
        myAnimator.ResetTrigger("Attack");
        myAnimator.ResetTrigger("Guard");
    }

    private void HandleJump()
    {
        if (secondJump || !myAnimator.GetCurrentAnimatorStateInfo(0).IsTag("CanJump")) return; //2 jumps max
        if (!jump) //the first jump
        {
            myAnimator.SetBool("Jump", true);
            jump = true;
            StartCoroutine(CheckUntilGrounded());
        }
        else //second jump
            secondJump = true;
        myRigidBody.velocity = new Vector2(myRigidBody.velocity.x, jumpForce);
    }

    private bool IsGrounded()
    {
        byte i = 0;
        for (; i < groundPoint.Length && !IsCollidingWithGround(i); ++i)
                ;//null statement
        return (i != groundPoint.Length);
    }

    private bool IsCollidingWithGround(byte index)
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(groundPoint[index].position, GROUND_RADIUS, whatIsGround);
        return (colliders.Length > 1); //1 collider if only coliding with layer. And more if coliding with ground
    }

    private void HandleMovement()
    {
        if (!myAnimator.GetCurrentAnimatorStateInfo(0).IsTag("Stop") && !usingThrustSkill)
        {
            myRigidBody.velocity = new Vector2(horizontalMovement * moveSpeed, myRigidBody.velocity.y);
            if (horizontalMovement > 0 && !FacingRight)
            {
                FacingRight = true;
                Flip();
                (auraShield.GetComponent(typeof(AuraShield)) as AuraShield).Flip();

            }
            else if (horizontalMovement < 0 && FacingRight)
            {
                FacingRight = false;
                Flip();
                (auraShield.GetComponent(typeof(AuraShield)) as AuraShield).Flip();
            }
        }
    }

    public new void TakeDamage(float damage)
    {
        if (!Shielding&&!AuraShield.IsActive)
        {
            base.TakeDamage(damage);
            AudioSource.PlayClipAtPoint(hurtSound, Camera.main.transform.position, 0.5f);

            fillBarManager.UpdateHealthFill(Life);
        }
    }

    override protected void TryAttack()
    {
        bool canAttack = !AttackCooldown && !ShopDoor.TouchingDoor && !Shielding && !usingSpellSkill && !usingThrustSkill;
        if (canAttack)
        {
            AttackCooldown = true;
            myAnimator.SetTrigger("Attack");
            AudioSource.PlayClipAtPoint(swordSound, Camera.main.transform.position, 0.5f);
            StartCoroutine(DoDamage());
        }
    }
    private void ProtectWithShield()
    { 
        bool notUsingAttacks =  !usingSpellSkill && !usingThrustSkill && !AttackCooldown;
       
        if (canShield && notUsingAttacks)
        {
            canShield = false;
            Shielding = true;
            myAnimator.SetTrigger("Guard");
            StartCoroutine(WaitEndShielding());
        }
    }

    private IEnumerator WaitEndShielding()
    {
        yield return new WaitForSeconds(0.1f);
        while (!myAnimator.GetCurrentAnimatorStateInfo(0).IsTag("CanJump"))
            yield return new WaitForEndOfFrame();
        Shielding = false;
        yield return new WaitForSeconds(0.5f);
        canShield = true;
    }

    protected override void Die()
    {
        myAnimator.SetTrigger("Die");
        (FindObjectOfType(typeof(LosePanel)) as LosePanel).Entry();
    }

    private IEnumerator DoDamage()
    {
        yield return new WaitForSeconds(ATTACK_ANIMATION_DURATION);
        
        Collider2D[] enemiesToDamage = Physics2D.OverlapAreaAll(damageZoneA.position, damageZoneB.position, whatIsEnemy);
        foreach (Collider2D enemyColider in enemiesToDamage)
            (enemyColider.GetComponent(typeof(Enemy)) as Enemy).TakeDamage(damage);
        //Wait for end of attack animation
        while (!myAnimator.GetCurrentAnimatorStateInfo(0).IsTag("CanJump"))
            yield return new WaitForEndOfFrame();
        AttackCooldown = false;


    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Vector2 pointA = damageZoneA.transform.position;
        Vector2 pointB = damageZoneB.transform.position;
        Vector2 pointC = new Vector3(damageZoneA.transform.position.x, damageZoneB.transform.position.y);
        Vector2 pointD = new Vector3(damageZoneB.transform.position.x, damageZoneA.transform.position.y);
        Gizmos.DrawLine(pointA, pointC);
        Gizmos.DrawLine(pointC, pointB);
        Gizmos.DrawLine(pointB, pointD);
        Gizmos.DrawLine(pointD, pointA);
    }

    private void ThrustSkill()
    {
        bool canThrust = !usingThrustSkill && !usingSpellSkill && !AttackCooldown && !Shielding;
        if (canThrust)
        {
            LoseManaPoint();
            sbyte direction = FacingRight ? (sbyte)1 : (sbyte)-1;
            float force = 20;

            thrusterImage.SetActive(true);
            usingThrustSkill = true; 
            myAnimator.SetTrigger("vSkill");
            AudioSource.PlayClipAtPoint(skillSound, Camera.main.transform.position, 1f);
            StartCoroutine(DoConstantDamage());
            myRigidBody.velocity = new Vector2(direction *force, myRigidBody.velocity.y);
            StartCoroutine(WaitForEndOfThrust());
        }
    }

    private void LoseManaPoint()
    {
        --ActualMana;
        fillBarManager.SubstractManaPointImage();
    }

    private IEnumerator DoConstantDamage()
    {
        List<Collider2D> enemiesAlreadyHitten = new List<Collider2D>();
        while(usingThrustSkill)
        {
            Collider2D[] enemiesToDamage = Physics2D.OverlapAreaAll(damageZoneA.position, damageZoneB.position, whatIsEnemy);
            foreach (Collider2D enemyColider in enemiesToDamage)
            {
                if(!enemiesAlreadyHitten.Contains(enemyColider))
                    (enemyColider.GetComponent(typeof(Enemy)) as Enemy).TakeDamage(damage/2);
                enemiesAlreadyHitten.Add(enemyColider);
            }
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator WaitForEndOfThrust()
    {
        yield return new WaitForSeconds(0.45f);
        myRigidBody.velocity = new Vector2(0, myRigidBody.velocity.y);
        thrusterImage.SetActive(false);
        while (!myAnimator.GetCurrentAnimatorStateInfo(0).IsTag("CanJump"))
            yield return new WaitForEndOfFrame();

        usingThrustSkill = false;
    }

    private void UpperCut()
    {
        bool canUpperCut = !usingSpellSkill && !usingThrustSkill && !sitting && !AttackCooldown && !Shielding;
        if (canUpperCut)
        {
            usingSpellSkill = true;
            LoseManaPoint();
            myAnimator.SetBool("UpperCut",true);
            UpperCutImage.SetActive(true);
            StartCoroutine(WaitForEndOfCasting());
        }
    }
    private IEnumerator WaitForEndOfCasting()
    {
        
        for (int i=0; !myAnimator.GetCurrentAnimatorStateInfo(0).IsTag("UpperCut") ;++i)
        {
            if (i >= 50) { usingSpellSkill = false; UpperCutImage.SetActive(false); HealMana(1); yield break; } //UNKNOW ERROR. SOMETIMES (USSUALLY WHEN PRESS D A LOT AND THEN USE THE UPPERCUT) THE UPPERCUT JUST DONT SHOOT
                                                                                                                //COULDNT SOLVE IT SO THIS BREAK THE LOOP AND RECOVER THE LOST MANA
            yield return new WaitForEndOfFrame();
        }

        UpperCutImage.SetActive(false);
        PlayerSpell spell = Instantiate(spellPrefab, transform.position, Quaternion.identity)   /*-------------*/   .GetComponent(typeof(PlayerSpell)) as PlayerSpell;
        spell.Right = FacingRight;
        spell.Damage = damage/2;

        while (!myAnimator.GetCurrentAnimatorStateInfo(0).IsTag("CanJump")){yield return new WaitForEndOfFrame();}
        myAnimator.SetBool("UpperCut", false);
        usingSpellSkill = false;
    }

    public void AddManaPoint()
    {
        if (ActualMana < MAX_MANA)
            ++ActualMana;
    }

    public void HealLife(byte value)
    {
        Life += value;
        fillBarManager.UpdateHealthFill(Life);
    }

    public void HealMana(byte value)
    {
        if (ActualMana + value > MAX_MANA)
            value = (byte)(MAX_MANA - ActualMana);
        ActualMana += value;
        fillBarManager.AddMana(value);
    }

    public void ActivateAuraShield()
    {
        (auraShield.GetComponent(typeof(AuraShield)) as AuraShield).ActivateAura();
    }
}

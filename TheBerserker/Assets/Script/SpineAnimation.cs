﻿using UnityEngine;
using Spine.Unity;


public class SpineAnimation : StateMachineBehaviour
    //SCRIPT USED IN THE ANIMATOR TO SET SOME CHARACTERISTICS OF THE ANIMATIONS
{
    [SerializeField] private string animationName;
    [SerializeField] private float speed;
    [SerializeField] private bool loop;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        SkeletonAnimation anim = animator.GetComponent(typeof(SkeletonAnimation)) as SkeletonAnimation;
        anim.state.SetAnimation(0, animationName, loop).TimeScale = speed; 
    }
}

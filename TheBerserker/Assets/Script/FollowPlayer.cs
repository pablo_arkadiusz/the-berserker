﻿using UnityEngine;

public class FollowPlayer : MonoBehaviour
    //SCRIPT THAT MAKE THE CAMERA FOLLOW PLAYER
{
    public float dampTime = 0.15f;
    private Vector3 velocity = Vector3.zero;
    public Transform target;

    private float yPos;
    private float initialYPlayerPos;

    private void Start()
    {
        initialYPlayerPos = target.position.y;
        float yCamPos = transform.position.y;
        float yPlayerPos = target.position.y;
        yPos =  yPlayerPos - yCamPos; 
    }

    private void FixedUpdate() //THE CAMERA FOLLOWS THE PLAYER HORIZONTALLY WITH DELAY.AND VERTICALLY ONLY IF THE PLAYER IS ADOBE OF THE 50% OF THE SCREEN
    {
        float yDesviation = 0;
        Vector3 playerRelativePos = GetComponent<Camera>().WorldToViewportPoint(target.position);
        if(playerRelativePos.y>0.5f) //IF THE PLAYER IS ADOBE OF THE 50% OF THE SCREEN
            yDesviation = (target.position.y - initialYPlayerPos) / 5;
        Vector3 targetPos = new Vector3(target.position.x, initialYPlayerPos + yDesviation, target.position.z);
        Vector3 point = GetComponent<Camera>().WorldToViewportPoint(targetPos);
        Vector3 delta = targetPos - GetComponent<Camera>().ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z));
        Vector3 deltaWithY = new Vector3(delta.x/2, (delta.y - yPos), delta.z);
        Vector3 destination = transform.position + deltaWithY;
        transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);
    }
}

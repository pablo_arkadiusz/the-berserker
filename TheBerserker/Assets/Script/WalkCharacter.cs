﻿using UnityEngine;

public class WalkCharacter : MonoBehaviour
    //SCRIPT THAT MANAGES THE CHARACTERS THAT WALK IN THE MENU SCENE
{
    [SerializeField] float moveSpeed;
    private Rigidbody2D myRigidBody;
    private Vector2 startPos;
    private void Start()
    {
        startPos = transform.position;
        myRigidBody = GetComponent(typeof(Rigidbody2D)) as Rigidbody2D;
        (GetComponent(typeof(Animator)) as Animator).SetBool("Run", true);
    }

    private void Update() {JustWalk();}
    private  void JustWalk() {myRigidBody.velocity = new Vector2(-moveSpeed, 0);}

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.layer != 8)
            transform.position = startPos;
    }


}
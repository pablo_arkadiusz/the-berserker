﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusItem : MonoBehaviour
    //BONUS ITEM GIVE BONIFICATION TO PLAYER ON TRIGGER. IT CAN BE MANA OR HEALTH
{
    private enum TypeOfBonus {Life,Mana };
    [SerializeField] TypeOfBonus typeOfBonus;
    [SerializeField] private AudioClip healSound;

    private byte lifeToAdd = 25;
    private byte manaToAdd = 2;


    private void OnTriggerEnter2D(Collider2D other) //JUST ADD THE BONUS, PLAY SOUND AND DESTROY ITSELF
    {
        PlayerCharacter player= other.GetComponent(typeof(PlayerCharacter)) as PlayerCharacter;
        if (typeOfBonus == TypeOfBonus.Life && player.Life != PlayerCharacter.MAX_LIFE)
        {
            AudioSource.PlayClipAtPoint(healSound, Camera.main.transform.position, .5f);
            (other.GetComponent(typeof(PlayerCharacter)) as PlayerCharacter).HealLife(lifeToAdd);
            Destroy(gameObject);
        }
        else if (typeOfBonus == TypeOfBonus.Mana && player.ActualMana != PlayerCharacter.MAX_MANA)
        {
            AudioSource.PlayClipAtPoint(healSound, Camera.main.transform.position, .5f);
            (other.GetComponent(typeof(PlayerCharacter)) as PlayerCharacter).HealMana(manaToAdd);
            Destroy(gameObject);
        }
       
    }
}

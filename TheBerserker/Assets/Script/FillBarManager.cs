﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FillBarManager : MonoBehaviour
    //SCRIPT THAT MANAGE THE FILL OF THE MANA AND HEALTH BAR
{
    
    [Header("Bars")]
    [SerializeField] private Image healthBar;
    [SerializeField] private Transform manaBarContainer;

    [Header("Texts")]
    [SerializeField] private Text coinTxt;
    [SerializeField] private Text shopCoinTxt;

    //MANA variables
    private const byte MAX_MANA_POINTS = 6;
    private float manaChargeTime=10;
    float chargeAmount;
    private byte actualManaPoints = 6;
    private Image[] manaPointImg;

    private PlayerCharacter player;
    
    void Start ()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent(typeof(PlayerCharacter)) as PlayerCharacter;
        UpdateCoinText();
        healthBar.fillAmount = 1;
        manaPointImg = manaBarContainer.GetComponentsInChildren<Image>();
        StartCoroutine(ChargeMana());
        chargeAmount = 0.1f / manaChargeTime;
    }
	
    public void UpdateHealthFill(float newLife){healthBar.fillAmount = newLife/PlayerCharacter.MAX_LIFE;}

    //when player sits
    public void IncreaseManaChargeSpeed() {chargeAmount = 0.05f / manaChargeTime * 3f;}
    public void BackToNormalManaChargeSpeed() {chargeAmount = 0.05f / manaChargeTime;}


    public void SubstractManaPointImage() //WHEN PLAYER USES ABILITIES
    {
        float loadedMana = 0;
        if (actualManaPoints != 6)
        {
            loadedMana = manaPointImg[actualManaPoints].fillAmount;
            manaPointImg[actualManaPoints].fillAmount = 0;
        }
        --actualManaPoints;
        manaPointImg[actualManaPoints].fillAmount = loadedMana;
    }

    private IEnumerator ChargeMana()
    {
        while(true)
        {
            if (actualManaPoints != 6 && manaPointImg[5].fillAmount < 1)
            {
                manaPointImg[actualManaPoints].fillAmount += chargeAmount;
                if (manaPointImg[actualManaPoints].fillAmount >= 1 && actualManaPoints != 6)
                {
                    ++actualManaPoints;
                    player.AddManaPoint();
                }
            }
            yield return new WaitForSeconds(0.05f);
        }
    }

    public void AddMana(byte amount)
    {
        byte manaToAdd;
        manaToAdd = amount + actualManaPoints > 6 ? (byte)(MAX_MANA_POINTS - actualManaPoints) : amount;
        if (actualManaPoints + manaToAdd < 6)
        {
            float manaCharged = manaPointImg[actualManaPoints].fillAmount;
            manaPointImg[actualManaPoints+manaToAdd].fillAmount = manaCharged;
        }
        for (byte i = 0; i < manaToAdd; ++i, ++actualManaPoints)
            manaPointImg[actualManaPoints].fillAmount = 1;
    }

    public void UpdateCoinText(){shopCoinTxt.text = coinTxt.text = player.PlayerCoins.ToString();}

}

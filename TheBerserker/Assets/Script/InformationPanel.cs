﻿using UnityEngine;
using UnityEngine.UI;

public class InformationPanel : MonoBehaviour
    //SCRIPT THAT MANAGE THE INFORMATION PANEL (PANEL THAT INDICATES THE TIME LEFT FOR THE NEXT WAVE, THE ENEMIES ALIVE, AND A CONGRATULATION MESSAGE)
{
    [SerializeField] private SpawningManager spawningManager;
    [SerializeField] private Animator myAnimator;
    [SerializeField] private Text enemiesLeftTitleTxt;
    [SerializeField] private Text enemiesLeftTxt;
    [SerializeField] private Text waveTxtTitle;
    [SerializeField] private Text waveNumberText;
    [SerializeField] private Text nextWaveInText;
    [SerializeField] private Text secsToNextWaveTxt;

    public void UpdateEnemiesLeft(uint newEnemies){enemiesLeftTxt.text = newEnemies.ToString();}
    public void ActiveMoveText() { waveNumberText.enabled = waveTxtTitle.enabled = true; }
    public void UpdateSecsLeftText(byte secLeft) { secsToNextWaveTxt.text = secLeft.ToString(); }
    public void UpdateActuaWaveTxt(string actualWave) { waveNumberText.text = actualWave + " completed!"; }
    public void GoTop() { myAnimator.SetTrigger("GoTop"); }


    public void Entry() //THE "ENTRY STATE" (WHEN THE PANEL INDICATES HOW MUCH ENEMIES ALIVE LEFT)
    {
        enemiesLeftTxt.enabled = enemiesLeftTitleTxt.enabled = true;
        myAnimator.SetTrigger("Entry");
    }
    public void Move() // THE "MOVE STATE" (WHEN THE PANEL GO TO THE CENTER OF THE SCREEN, WHEN WAVE END AND IT SEND A CONTRATULATION MESSAGE)
    {
        enemiesLeftTxt.enabled = enemiesLeftTitleTxt.enabled = false;
        myAnimator.SetTrigger("Move");
    }
    public void ActiveTopText() //THE "TOP STATE" (WHEN THE PANEL GOES TOP ADN START COUNTING THE TIME LEFT TO THE NEXT WAVE)
    {
        waveNumberText.enabled = waveTxtTitle.enabled = false;
        nextWaveInText.enabled = secsToNextWaveTxt.enabled = true;
    }
    public void Exit()
    {
        nextWaveInText.enabled = secsToNextWaveTxt.enabled = false;
        myAnimator.SetTrigger("Exit");
    }
}

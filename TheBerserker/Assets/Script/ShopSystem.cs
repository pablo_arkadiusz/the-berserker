﻿using UnityEngine;
using UnityEngine.UI;

public class ShopSystem : MonoBehaviour
    //SCRIPT THAT MANAGES THE SHOPPING IN THE STORE
{
    [SerializeField] private Transform spawnZone;
    [SerializeField] private GameObject swordsManPrefab;

    [Header("Shield")]
    [SerializeField] private int shieldPrice;
    [SerializeField] private Button shieldPriceBtn;
    [SerializeField] private Text shieldPriceTxt;
    [SerializeField] private FillBarManager fillBarManager;

    [Header("Soldier")]
    [SerializeField] private int soldierPrice;
    [SerializeField] private Button soldierBuyBtn;
    [SerializeField] private Button rightBtn;
    [SerializeField] private Button leftBtn;
    [SerializeField] private Text soldierNumberTxt;
    [SerializeField] private Text soldierPriceTxt;
    private int soldierNumber = 0;
    private int finalSoldierPrice;

    [Header("Upgrade")]
    [SerializeField] private int upgradePrice;
    [SerializeField] private Button upgradeBtn;
    [SerializeField] private Transform lightParent;
    [SerializeField] private Text upgrPriceTxt;

    private PlayerCharacter player;

    private void Start()
    {
        player = FindObjectOfType(typeof(PlayerCharacter)) as PlayerCharacter;
        UpdateSoldierTexts();
        shieldPriceTxt.text = shieldPrice.ToString();
        upgrPriceTxt.text = upgradePrice.ToString();  
    }

    public void UpdateShopButtonsState()
    {
        shieldPriceBtn.interactable = (player.PlayerCoins >= shieldPrice && !AuraShield.IsActive) ? true : false;
        upgradeBtn.interactable = (player.PlayerCoins >= upgradePrice && player.Upgrade<3) ? true : false;
        UpdateUpgradeLights();
        ResetSoldierValues();
    }

    private void SpawnSwordsMan()
    {
        for (byte i = 0; i < soldierNumber; ++i)
        {
            GameObject swordsMan = Instantiate(swordsManPrefab);
            swordsMan.transform.parent = spawnZone.parent;
            Vector3 spawningPos = new Vector3(spawnZone.localPosition.x, spawnZone.localPosition.y, -0.2f);
            swordsMan.transform.localPosition = spawningPos;
        }
    }

    public void BuySoldiers()
    {
        player.PlayerCoins -= finalSoldierPrice;
        fillBarManager.UpdateCoinText();
        SpawnSwordsMan();
        UpdateShopButtonsState();
    }

    private void ResetSoldierValues()
    {
        soldierNumber = 0;
        finalSoldierPrice = 0;
        UpdateSoldierButtonInteractibility();
        UpdateSoldierTexts();
    }

    public void BuyShield()
    {
        
        if (player.PlayerCoins >= shieldPrice)
        {
            player.PlayerCoins -= shieldPrice;
            fillBarManager.UpdateCoinText();
            player.ActivateAuraShield();
            shieldPriceBtn.interactable = false;
            UpdateShopButtonsState();
        }

    }

    private void UpdateSoldierButtonInteractibility()
    {
        soldierBuyBtn.interactable = leftBtn.interactable = (soldierNumber == 0) ? false : true;
        rightBtn.interactable = (player.PlayerCoins >= finalSoldierPrice + soldierPrice) ? true : false;
    }

    private void UpdateSoldierTexts()
    {
        soldierNumberTxt.text = soldierNumber.ToString();
        soldierPriceTxt.text = (soldierNumber * soldierPrice).ToString();
    }


    public void AddSoldier()
    {
        ++soldierNumber;
        finalSoldierPrice += soldierPrice;
        UpdateSoldierButtonInteractibility();
        UpdateSoldierTexts();
    }


    public void RemoveSoldier()
    {
        --soldierNumber;
        finalSoldierPrice -= soldierPrice;
        UpdateSoldierButtonInteractibility();
        
        UpdateSoldierTexts();
    }


    public void UpgradePlayer()
    {
        player.PlayerCoins -= upgradePrice;
        if (player.PlayerCoins < upgradePrice) upgradeBtn.interactable = false;
        fillBarManager.UpdateCoinText();
        ++player.Upgrade;
        UpdateShopButtonsState();
    }

    private void UpdateUpgradeLights()
    {
        for (byte i = 0; i < 3; ++i)
            (lightParent.GetChild(i).GetComponent(typeof(Image)) as Image).sprite = (player.Upgrade == i + 1) ?
                Resources.Load<Sprite>("ON") : Resources.Load<Sprite>("OFF");
    }
}

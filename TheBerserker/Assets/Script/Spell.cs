﻿using System.Collections;
using UnityEngine;

public class Spell : MonoBehaviour
{
    [SerializeField] private float speed;
    public float Damage { set; get; }
    private Transform target;
    private bool derecha;
    private float yDirection;

    private float xDirection;
    public void SetTarget(Transform newTarget) {target = newTarget;}

	void Start ()
    {
        StartCoroutine(DieInXSecs(3));
        derecha = (transform.position.x < target.position.x) ? true : false;
        Flip();

        Vector3 shoot = (target.position - transform.position).normalized;
        (GetComponent(typeof(Rigidbody2D)) as Rigidbody2D).AddForce(shoot * speed);

        Vector3 difference = target.position - transform.position;
        float rotationZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0.0f, 0.0f, rotationZ);
    }

    protected void Flip()
    {
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    private IEnumerator DieInXSecs(float sec)
    {
        yield return new WaitForSeconds(sec);
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == 8 || other.gameObject.layer == 16) return;
        PlayerCharacter playerChar = other.GetComponent(typeof(PlayerCharacter)) as PlayerCharacter;
        if (playerChar)
            playerChar.TakeDamage(Damage);
        else
            (other.GetComponent(typeof(Character)) as Character).TakeDamage(Damage);
        Destroy(gameObject);
    }
}

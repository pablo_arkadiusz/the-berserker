﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner : MonoBehaviour
{
    [SerializeField] private Transform lifeBonusPrefab;
    [SerializeField] private Transform manaBonusPrefab;
    [SerializeField] private Transform[] spawnPoints;

    private GameObject lastLifeSpawned,lastManaSpawned;

    public void SpawnLifeAndManaBonus()
    {
        byte randomIndexHealth = (byte)Random.Range(0, spawnPoints.Length - 1);
        byte randomIndexMana;
        //The mana index have to be different to the health index
        do
            randomIndexMana = (byte)Random.Range(0, spawnPoints.Length - 1);
        while (randomIndexMana == randomIndexHealth);

        Transform positionToSpawn = spawnPoints[randomIndexHealth];
        lastLifeSpawned = Instantiate(lifeBonusPrefab, positionToSpawn.position, Quaternion.identity).gameObject;

        positionToSpawn = spawnPoints[randomIndexMana];
        lastManaSpawned = Instantiate(manaBonusPrefab, positionToSpawn.position, Quaternion.identity).gameObject;

        lastLifeSpawned.transform.parent = transform;
        lastManaSpawned.transform.parent = transform;
    }

    public void DeleteLastSpawnedItems()
    {
        if (lastLifeSpawned != null)
            Destroy(lastLifeSpawned);
        if (lastManaSpawned != null)
            Destroy(lastManaSpawned);
    }
}

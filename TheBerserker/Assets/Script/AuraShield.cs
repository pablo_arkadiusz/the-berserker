﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AuraShield : MonoBehaviour
    //AURA SHIELD THAT CAN BE BOUGHT IN SHOP TO PROTECT PLAYER FROM DAMAGE
{
   
    [SerializeField] private Image healthImage;

    private float life = 3;
    private const float maxLife = 3;
    static public bool IsActive { private set; get; } //when AuroShield is active Player is protected from damage

    public void ActivateAura()
    {
        IsActive = true;
        gameObject.SetActive(true);
        life = maxLife;
        healthImage.fillAmount = life / maxLife;
    }

    private void TakeDamage()
    {
        --life;
        healthImage.fillAmount = life / maxLife; 
        if (life <= 0)
        {
            IsActive = false;
            gameObject.SetActive(false);
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        TakeDamage();
        Destroy(other.gameObject);
    }

    public void Flip() //To have always the same orientation, when player flips this also flip. So with this method it flip twice to mantain the orientation
    {
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    } 
}

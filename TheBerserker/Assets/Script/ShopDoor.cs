﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopDoor : MonoBehaviour
    //THE SCRIPT THAT MANAGE THE OPEN/CLOSE OF THE SHOP
{
    [SerializeField] private Animator shopAnimator;

    static public bool TouchingDoor { private set; get; }
    static public bool shopPause;
    private bool allowOpenShop = true;

    private void Awake(){shopPause = false;}

    private void OnTriggerEnter2D(Collider2D collision)
    //IF THE PLAYER TOUCH THE DOOR, THE SHOP CAN BE OPEN ON MOUSECLICK
    {
        TouchingDoor = true;
        StartCoroutine(WaitForOpenDoor());
    }

    private void OnTriggerExit2D(Collider2D collision){TouchingDoor = false;}

    
    private IEnumerator WaitForOpenDoor() //THE SHOP IS OPEN ON MOUSECLICK
    {
        while(TouchingDoor)
        {
            if (Input.GetMouseButton(0)&&allowOpenShop)  //GetMouseButtonDown is bugging, i think because is a coroutine and not update. So i used auxiliar bool to do only once
            {
                allowOpenShop = false;
                (shopAnimator.GetComponent(typeof(ShopSystem)) as ShopSystem).UpdateShopButtonsState();
                shopAnimator.SetBool("OpenShop", true);
                Time.timeScale = 0f;
                shopPause = true;
            }
            yield return new WaitForEndOfFrame();
        }
    }

    public void CloseShop()
    {
        if(!PauseMenu.GamePaused) Time.timeScale = 1f;
        allowOpenShop = true;
        shopAnimator.SetBool("OpenShop", false);
        shopPause = false;
    }
}


